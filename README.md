# yabai-summon-space
A terrible and hacky workspace switching mechanism for yabai. When you summon a space, that space is brought directly to you. If that space happens to be on another monitor, then your active workspace will be moved to that monitor to take the place of the summoned space.

## Installation
```shell
stack install
```

## Usage
```shell
yabai-summon-space <space label or index>
```
