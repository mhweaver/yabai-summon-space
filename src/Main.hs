{-# LANGUAGE OverloadedStrings #-}
module Main where

import System.Environment
import System.Exit
import System.Process.Typed
import Control.Monad
import Data.Char (isDigit)
import Data.Aeson
import Control.Applicative (empty)

data Space = Space { spaceId :: Int
                   , label :: String
                   , index:: Int
                   , displayIndex :: Int
                   , focused :: Int
                   , nativeFullscreen :: Int
}
instance FromJSON Space where
  parseJSON (Object v) = Space <$>
                         v .: "id" <*>
                         v .: "label" <*>
                         v .: "index" <*>
                         v .: "display" <*>
                         v .: "focused" <*>
                         v .: "native-fullscreen"
  parseJSON _          = empty

getSpaces :: IO (Maybe [Space])
getSpaces = do
  (rawSpaces, _) <- readProcess_ "yabai -m query --spaces"
  let spaces = decode rawSpaces :: Maybe [Space]
  return spaces

getSpacesOnCurrentDisplay = do
  (rawSpaces, _) <- readProcess_ "yabai -m query --spaces --display"
  let (Just spaces) = decode rawSpaces :: Maybe [Space]
  return spaces

createSpace :: IO (Int)
createSpace = do
  runProcess "yabai -m space --create"
  spacesOnDisplay <- getSpacesOnCurrentDisplay
  return . spaceId $ last . filter (\s -> nativeFullscreen s == 0) $ spacesOnDisplay

destroySpace spaceId spaces = do
  let index = getSpaceIndexFromId spaceId spaces
  runProcess . shell $ "yabai -m space --destroy " ++ (show index)

getSpaceIndexFromId :: Int -> [Space] -> Int
getSpaceIndexFromId targetId = index . head . filter (\s -> spaceId s == targetId)

focusSpace targetId spaces = do
  let index = getSpaceIndexFromId targetId spaces
  runProcess $ shell $ "yabai -m space --focus " ++ (show index)

moveSpaceToDisplay spaceId display spaces = do
  let index = getSpaceIndexFromId spaceId spaces
  runProcess $ shell $ "yabai -m space " ++ (show index) ++ " --display " ++ (show display)

main :: IO ()
main = do
  (targetSpaceArg:_) <- getArgs

  (Just spaces) <- getSpaces
  let targetSpacePred = if all isDigit targetSpaceArg
                        then (\s -> index s == read targetSpaceArg)
                        else (\s -> label s == targetSpaceArg)
      targetSpace = head . filter targetSpacePred $ spaces
  when (focused targetSpace == 1) $ die "space is already focused"

  let targetDisplay = displayIndex targetSpace
      targetSpaceId = spaceId targetSpace
      initialSpace = head . filter (\s -> focused s == 1) $ spaces
      initialSpaceId = spaceId initialSpace
      initialDisplay = displayIndex initialSpace

  when (targetDisplay /= initialDisplay) $ do
    -- Create a temp space
    tempSpaceId <- createSpace
    (Just spaces) <- getSpaces
    -- Focus the temp space
    focusSpace tempSpaceId spaces
    -- Move the current space to the target space's display
    moveSpaceToDisplay initialSpaceId targetDisplay spaces
    (Just spaces) <- getSpaces
    -- Move the target space to this display
    moveSpaceToDisplay targetSpaceId initialDisplay spaces
    -- Remove the temp space
    (Just spaces) <- getSpaces
    destroySpace tempSpaceId spaces
    exitSuccess

  focusSpace targetSpaceId spaces
  exitSuccess

